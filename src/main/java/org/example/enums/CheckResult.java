package org.example.enums;

/**
 * CheckResult.
 *
 * @author Tatyana_Dolnikova
 */
public enum CheckResult {
    ACCEPT,
    DECLINE,
    CHALLENGE;
}
