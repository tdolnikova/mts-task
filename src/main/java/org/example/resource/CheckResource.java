package org.example.resource;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * CheckResource.
 *
 * @author Tatyana_Dolnikova
 */
@RequestMapping(CheckResource.URL)
public interface CheckResource {

    String URL = "/check";

    @PostMapping
    ResponseEntity<String> checkPhoneNumber(@RequestParam String phoneNumber);

}
