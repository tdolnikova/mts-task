package org.example.resource;

import lombok.extern.slf4j.Slf4j;
import org.example.enums.CheckResult;
import org.example.service.CheckService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

/**
 * CheckResourceImpl.
 *
 * @author Tatyana_Dolnikova
 */
@Slf4j
@RestController
public class CheckResourceImpl implements CheckResource {

    @Autowired
    private CheckService checkService;

    @Override
    public ResponseEntity<String> checkPhoneNumber(String phoneNumber) {
        System.out.println("checkPhoneNumber(): phoneNumber=" + phoneNumber);
        log.info("checkPhoneNumber(): phoneNumber={}", phoneNumber);
        CheckResult checkResult = checkService.checkPhoneNumber(phoneNumber);
        return checkResult == null ? ResponseEntity.badRequest().build() : ResponseEntity.ok(checkResult.name());
    }
}
