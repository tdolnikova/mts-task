package org.example.repository;

import org.example.entity.BPhoneNumber;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * PhoneNumberRepository.
 *
 * @author Tatyana_Dolnikova
 */
@Repository
public interface BPhoneNumberRepository extends JpaRepository<BPhoneNumber, Long> {

    @Query(value = "SELECT * FROM mts.phone_number_B as pn WHERE pn.tel_num = ?1", nativeQuery = true)
    List<BPhoneNumber> findByPhoneNumber(@Param("phoneNumber") String phoneNumber);

}
