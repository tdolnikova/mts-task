package org.example.repository;

import org.example.entity.APhoneNumber;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * PhoneNumberRepository.
 *
 * @author Tatyana_Dolnikova
 */
@Repository
public interface APhoneNumberRepository extends JpaRepository<APhoneNumber, Long> {

    @Query(value = "SELECT * FROM mts.phone_number_A as pn WHERE pn.tel_num = ?1", nativeQuery = true)
    List<APhoneNumber> findByPhoneNumber(@Param("phoneNumber") String phoneNumber);

}
