package org.example.service;

import liquibase.util.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.example.enums.CheckResult;
import org.example.entity.APhoneNumber;
import org.example.entity.BPhoneNumber;
import org.example.repository.APhoneNumberRepository;
import org.example.repository.BPhoneNumberRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;

/**
 * CheckService.
 *
 * @author Tatyana_Dolnikova
 */
@Slf4j
@Service
public class CheckService {

    @Autowired
    private APhoneNumberRepository aRepository;

    @Autowired
    private BPhoneNumberRepository bRepository;

    private static final int ALLOWED_LENGTH = 10;
    private static final String ALLOWED_SYMBOLS = "[0-9]+";

    public CheckResult checkPhoneNumber(String phoneNumber) {
        if (!isValid(phoneNumber)) {
            return null;
        }

        List<APhoneNumber> aPhoneNumbers = aRepository.findByPhoneNumber(phoneNumber);
        List<BPhoneNumber> bPhoneNumbers = bRepository.findByPhoneNumber(phoneNumber);
        if (CollectionUtils.isEmpty(aPhoneNumbers) && CollectionUtils.isEmpty(bPhoneNumbers)) {
            return CheckResult.ACCEPT;
        } else if (!CollectionUtils.isEmpty(aPhoneNumbers) && !CollectionUtils.isEmpty(bPhoneNumbers)) {
            return CheckResult.DECLINE;
        } else {
            return CheckResult.CHALLENGE;
        }
    }

    private boolean isValid(String phoneNumber) {
        if (StringUtils.isEmpty(phoneNumber)) {
            log.info("phoneNumber is null or empty");
            return false;
        }
        if (phoneNumber.trim().length() != ALLOWED_LENGTH) {
            log.info("phoneNumber length is not equal to {}", ALLOWED_LENGTH);
            return false;
        }
        if (!phoneNumber.matches(ALLOWED_SYMBOLS)) {
            log.info("phoneNumber doesn't match the allowed symbols {}", ALLOWED_SYMBOLS);
            return false;
        }
        return true;
    }

}
