package org.example.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;

/**
 * PhoneNumber B.
 *
 * @author Tatyana_Dolnikova
 */
@Entity
public class BPhoneNumber extends PhoneNumber {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @Column(name = "tel_num")
    String telNum;

    @Column(name = "add_date")
    Date addDate;

}