package org.example.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * PhoneNumber A.
 *
 * @author Tatyana_Dolnikova
 */
@Entity
public class APhoneNumber extends PhoneNumber {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @Column(name = "tel_num")
    String telNum;

    @Column(name = "add_date")
    Date addDate;

}